`timescale 1ns / 1ps

module Sqrt(
input clk_i ,
input rst_i ,
input [ 7 : 0 ] a_bi ,
input start_i ,
output busy_o ,
output reg [ 15 : 0 ] y_bo
) ;
	localparam IDLE = 1'b0 ;
	localparam WORK = 1'b1 ;
	
	reg [6:0] m;
	reg [15:0] y;
	reg [7:0] x,b;
	reg state;

	assign busy_o = state;
	
	always @(posedge clk_i )
		if ( rst_i ) begin
			y_bo <= 0;
			state <= IDLE;
		end else begin case ( state )
			IDLE :
				if ( start_i ) begin
					state <= WORK;
					x <= a_bi;
					m <= 7'b1000000;    //reset
					y <= 0;            //reset
					b <= 0;            //reset
				end
			WORK:
			begin
				if ( m == 0 ) begin
					state = IDLE;
					y_bo = y;
				end else begin 
				    b = y|m;
				    y = y >> 1;
				    if(x>=b) begin
				        x = x-b;
				        y = y|m;
				    end 
				    m = m>>2;
				end
			end
		endcase
	end
endmodule

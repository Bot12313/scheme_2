`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/15/2021 02:57:21 AM
// Design Name: 
// Module Name: Sqr
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Sqr(
input clk_i,
input rst_i,
input [ 7 : 0 ] a_bi ,
input start_i,
output busy_o,
output reg [ 15 : 0 ] y_bo
) ;
	localparam IDLE = 1'b0 ;
	localparam WORK = 1'b1 ;
	reg [ 7 : 0 ] a;
	reg state ;
	assign busy_o = state;
	
	reg start_mult;
	wire inner_busy;
	wire [15:0] mult_result;
	Multiply mult(
        .clk_i(clk_i),
        .rst_i(rst_i),
        .a_bi(a),
		.b_bi(a),
        .start_i(start_mult),
        .busy_o(inner_busy),
        .y_bo(mult_result)
	);
	
	always @(posedge clk_i )
		if ( rst_i ) begin
			y_bo <= 0 ;
			state <= IDLE;
		end else begin case ( state )
			IDLE :
				if ( start_i ) begin
				    start_mult <= 1; // start multiplying
					state <= WORK;
					a <= a_bi;
				end
			WORK:
			begin
				if ( start_i == 0 && start_mult == 0 && inner_busy == IDLE ) begin
					state = IDLE;
					y_bo = mult_result;
				end
			    start_mult = 0; // no need to start it again
			end
		endcase
	end
endmodule

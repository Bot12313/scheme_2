`timescale 1ns / 1ps

module Main(
input clk_i ,
input rst_i ,
input [ 7 : 0 ] a_bi ,
input [ 7 : 0 ] b_bi ,
input start_i ,
output busy_o ,
output reg [ 15 : 0 ] y_bo
);
	localparam IDLE = 1'b0 ;
	localparam WORK = 1'b1 ;
	reg [ 7 : 0 ] a,b;
	reg state ;
	assign busy_o = state;
	
    reg start_sqr;
	wire inner_busy1;
	wire [15:0] sqr_result;
	Sqr sqr1(
        .clk_i(clk_i),
        .rst_i(rst_i),
        .a_bi(a),
        .start_i(start_sqr),
        .busy_o(inner_busy1),
        .y_bo(sqr_result)
	);
	
    reg start_sqrt;
	wire inner_busy2;
	wire [15:0] sqrt_result;
	Sqrt sqrt_(
        .clk_i(clk_i),
        .rst_i(rst_i),
        .a_bi(b),
        .start_i(start_sqrt),
        .busy_o(inner_busy2),
        .y_bo(sqrt_result)
    );
	
	always @(posedge clk_i )
		if ( rst_i ) begin
			y_bo <= 0 ;
			state <= IDLE; 
		end else begin case ( state )
			IDLE :
				if ( start_i ) begin
				    start_sqr <= 1; // start 
				    start_sqrt <= 1; // start
					state <= WORK;
					a <= a_bi;
					b <= b_bi;
				end
			WORK:
			begin
				if ( start_sqr == 0 && start_sqrt == 0 && inner_busy1 == IDLE && inner_busy2 == IDLE ) begin
					state <= IDLE;
					y_bo <= sqr_result + sqrt_result;
				end
			    start_sqr <= 0; // no need to start it again
			    start_sqrt <= 0; // no need to start it again
			end
		endcase
	end
endmodule
